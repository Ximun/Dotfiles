gitCloneOrPull() {
  REPO_SRC=$1
  REPO_DST=$2
  OPTS=$3
  if [ ! -d $REPO_DST ]; then
    git clone $REPO_SRC $REPO_DST $OPTS
  else
    git -C $REPO_DST pull
  fi
}


# Kitty
gitCloneOrPull "https://github.com/catppuccin/kitty.git" "~/.config/kitty/themes/catppuccin"

# Waybar
gitCloneOrPull "https://github.com/catppuccin/waybar.git" "~/.config/waybar/themes/catppuccin"

# Rofi
gitCloneOrPull "https://github.com/catppuccin/rofi.git" "~/.config/rofi/themes"

# Dunst
gitCloneOrPull "https://github.com/catppuccin/dunst.git" "~/.config/dunst/themes"

# NvCHAD
gitCloneOrPull "https://github.com/NvChad/NvChad" "~/.config/nvim" "--depth 1"

#zsh-autosuggestions
gitCloneOrPull "https://github.com/zsh-users/zsh-autosuggestions" "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions"

# Godot 3.5.2 LTS
GODOT3_VERSION=3.5.2
GODOT3_BIN_PATH=/usr/local/bin/godot3
if [[ ! -f "$GODOT3_BIN_PATH" ]]; then
  wget -O /tmp/Godot_${GODOT3_VERSION}.zip https://github.com/godotengine/godot/releases/download/${GODOT3_VERSION}-stable/Godot_v${GODOT3_VERSION}-stable_x11.64.zip
  unzip /tmp/Godot_${GODOT3_VERSION}.zip -d ~/.local/bin/
  sudo ln -s ~/.local/bin/Godot_v3.5.2-stable_x11.64 /usr/local/bin/godot3
fi

# GTK theme
THEME_DIR=/usr/share/themes
GTK_THEME=Catppuccin-Macchiato-Standard-Sky-Dark
THEME_PATH="$THEME_DIR/$GTK_THEME"
if [[ -d "$THEME_PATH" ]]; then
  mkdir -p "~/.config/gtk-4.0"
  rm -f ~/.config/gtk-4.0/assets; ln -sf ${THEME_PATH}/gtk-4.0/assets ~/.config/gtk-4.0/assets
  ln -sf ${THEME_PATH}/gtk-4.0/gtk.css ~/.config/gtk-4.0/gtk.css
  ln -sf ${THEME_PATH}/gtk-4.0/gtk-dark.css ~/.config/gtk-4.0/gtk-dark.css
  rm -f ~/.config/gtk-3.0/assets; ln -sf ${THEME_PATH}/gtk-3.0/assets ~/.config/gtk-3.0/assets
  ln -sf ${THEME_PATH}/gtk-3.0/gtk.css ~/.config/gtk-3.0/gtk.css
  ln -sf ${THEME_PATH}/gtk-3.0/gtk-dark.css ~/.config/gtk-3.0/gtk-dark.css
else
  echo "=> Install ${GTK_THEME} on AUR before setup"
fi

papirus-folders -C cat-mocha-sky -t Papirus
